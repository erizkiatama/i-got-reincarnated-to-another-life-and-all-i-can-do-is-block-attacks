extends RigidBody2D

const RESTORE = 20

func _on_Effect_body_entered(body):
	if "Monster" in body.name:
		queue_free()
	elif body.name == "Player":
		body.hp += RESTORE
		if body.hp >= 50:
			body.hp = 50
		queue_free()
