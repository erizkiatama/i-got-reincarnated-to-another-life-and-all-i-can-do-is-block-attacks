extends Node2D

var Potion = preload("res://scene/Potion.tscn")
var BadPotion = preload("res://scene/Bad Potion.tscn")
var cave = preload("res://scene/Cave.tscn").instance()
onready var player = get_node("Player")
onready var dialog_box = get_node("Forest/Dialog Box")
onready var text = get_node("Forest/Dialog Box/RichTextLabel")
onready var player_sprite = get_node("Forest/Dialog Box/Player Sprite")
onready var goddess_sprite = get_node("Forest/Dialog Box/Goddess Sprite")

var cave_dialog = ["A cave, huh?", "Hey! I sense a valuable treasure in this cave!", "Whoa!",
"Don't surprise me like that!", "But, what did you say? There is a treasure inside?", 
"Yeah, I sense a very valuable treasure inside this cave.", "Hmm, alright. Let's go inside then!", 
"Yeah!!"]
var cave_turn = [0,1,0,0,0,1,0,1]

var ground_dialog = ["I think that ground is stable enough for me to pass by..."]
var ground_turn = [0]

var shaky_dialog = ["But it still shaky though. If it's something more heavy, it will collapse for sure."]
var shaky_turn = [0]

var boss_dialog = ["WHAT IS THAT THING?!", "A High Orc!?", 
"This is not good, against him I think you will take damage even if you are defending",
"Gah, you said I'm invincible!!", "Wait this isn't the time to shouting!",
"You have to find a way somehow to defeat it.", "Here it goes!!"]
var boss_turn = [0,1,1,0,0,1,0]

var index = 0
var is_cave_dialog = false
var is_ground_dialog = false
var is_shaky_dialog = false
var is_boss_dialog = false
var is_forest_visible = true

func _ready():
	if is_forest_visible:
		var distance = 1024
		var max_distance = 9000
		for i in range (5):
			randomize()
			var potion = Potion.instance()
			var bad_potion = BadPotion.instance()
			potion.position = Vector2(range(distance,max_distance)[randi()%range(distance,max_distance).size()], 100)
			bad_potion.position = Vector2(range(distance,max_distance)[randi()%range(distance,max_distance).size()], 100)
			if potion.position.x == bad_potion.position.x:
				bad_potion.position = Vector2(range(distance,max_distance)[randi()%range(distance,max_distance).size()], 100)
			add_child(potion)
			add_child(bad_potion)
			distance += 1000
	set_process_input(true)
	$Interface/Lives.text = "Lives   : " + str(player.lives)
	$Interface/Health.text = "Health  : " + str(player.hp)
	
func _physics_process(delta):
	$Interface/Lives.text = "Lives   : " + str(player.lives)
	$Interface/Health.text = "Health  : " + str(player.hp)
	
	if player.hp <= 0 && !player.is_dead:
		player.lives-1
		player.is_dead = true
		player.after_death.start()

func _on_Cave_Entrance_body_entered(body):
	if body == player:
		player.hide()
		player.is_talking = true
		dialog_box.global_position.x = $Forest/CaveDialogBoxSpawn.global_position.x		
		dialog_box.show()
		text.show()
		text.set_bbcode(cave_dialog[index])
		text.set_visible_characters(0)
		show_sprites(cave_turn)
		is_cave_dialog = true
		
func show_sprites(turn):
	if turn[index] == 0:
		player_sprite.show()
		goddess_sprite.hide()
	else:
		player_sprite.hide()
		goddess_sprite.show()
		
func _input(event):
	if (event is InputEventMouseButton && event.is_pressed()) || (event is InputEventKey && event.scancode == KEY_SPACE && event.is_pressed()):
		if text.get_visible_characters() > text.get_total_character_count():
			if index < cave_dialog.size() - 1 && is_cave_dialog:
				index += 1
				show_sprites(cave_turn)
				text.set_bbcode(cave_dialog[index])
				text.set_visible_characters(0)
			elif index == cave_dialog.size() - 1 && is_cave_dialog:
				index = 0
				is_cave_dialog = false
				dialog_box.hide()
				text.hide()
				player.show()
				player.is_talking = false
				$"Forest/Cave Entrance".queue_free()
				$"Forest/Trap Area 1".queue_free()
				$"Forest/Bat Monster".is_knocked = true
				$"Forest/Minotaur Monster".is_knocked = true
				$"Forest/Minotaur Monster".is_dead = true
				$Forest.hide()
				is_forest_visible = false
				add_child(cave)
				player.global_position = $Forest/PlayerSpawnPoint.global_position
			if index == ground_dialog.size() - 1 && is_ground_dialog:
				index = 0
				is_ground_dialog = false
				dialog_box.hide()
				text.hide()
				player.show()
				player.is_talking = false
				$"Forest/Trap Area 6".queue_free()
			if index == shaky_dialog.size() - 1 && is_shaky_dialog:
				index = 0
				is_shaky_dialog = false
				dialog_box.hide()
				text.hide()
				player.show()
				player.is_talking = false
				$"Forest/Trap Area 5".queue_free()
			if index < boss_dialog.size() - 1 && is_boss_dialog:
				index += 1
				show_sprites(boss_turn)
				text.set_bbcode(boss_dialog[index])
				text.set_visible_characters(0)
			elif index == boss_dialog.size() - 1 && is_boss_dialog:
				index = 0
				is_boss_dialog = false
				dialog_box.hide()
				text.hide()
				player.show()
				player.is_talking = false
				$"Forest/Trap Area 7".queue_free()
		else:
			text.set_visible_characters(text.get_total_character_count())

func _on_Dialog_Interval_timeout():
	text.set_visible_characters(text.get_visible_characters() + 1)

func _on_Trap_Area_1_body_entered(body):
	if body == player:
		$"Forest/Trap Area 1/Unstable Ground A1-1".queue_free()
		$"Forest/Trap Area 1/Unstable Ground A1-2".queue_free()
		$"Forest/Trap Area 1".queue_free()
		player.hp = 0
		player.after_death.start()


func _on_Trap_Area_2_body_entered(body):
	if body == player:
		$"Forest/Trap Area 2/Unstable Ground A2-1".queue_free()
		$"Forest/Trap Area 2/Unstable Ground A2-2".queue_free()
		$"Forest/Trap Area 2".queue_free()
		player.hp = 0
		player.after_death.start()


func _on_Trap_Area_3_body_entered(body):
	if body == player:
		$"Forest/Trap Area 3/Unstable Ground A3-1".queue_free()
		$"Forest/Trap Area 3/Unstable Ground A3-2".queue_free()
		$"Forest/Trap Area 3".queue_free()
		player.hp = 0
		player.after_death.start()


func _on_Trap_Area_4_body_entered(body):
	if body == player:
		$"Forest/Trap Area 4/Unstable Ground A4-1".queue_free()
		$"Forest/Trap Area 4/Unstable Ground A4-2".queue_free()
		$"Forest/Trap Area 4/Unstable Ground A4-3".queue_free()		
		$"Forest/Trap Area 4".queue_free()
		player.hp = 0
		player.after_death.start()

func _on_Trap_Area_5_body_entered(body):
	if body == player:
		player.hide()
		player.is_talking = true
		dialog_box.global_position.x = $Forest/ShakyDialogBoxSpawn.global_position.x
		dialog_box.show()
		text.show()
		text.set_bbcode(shaky_dialog[index])
		text.set_visible_characters(0)
		show_sprites(shaky_turn)
		is_shaky_dialog = true

func _on_Trap_Area_6_body_entered(body):
	if body == player:
		player.hide()
		player.is_talking = true
		dialog_box.global_position.x = $Forest/GroundDialogBoxSpawn.global_position.x
		dialog_box.show()
		text.show()
		text.set_bbcode(ground_dialog[index])
		text.set_visible_characters(0)
		show_sprites(ground_turn)
		is_ground_dialog = true

func _on_Trap_Area_7_body_entered(body):
	if body == player:
		player.hide()
		player.is_talking = true
		dialog_box.global_position.x = $Forest/BossDialogBoxSpawn.global_position.x
		dialog_box.show()
		text.show()
		text.set_bbcode(boss_dialog[index])
		text.set_visible_characters(0)
		show_sprites(boss_turn)
		is_boss_dialog = true

func _on_Trap_Area_8_body_entered(body):
	if "Boss" in body.name:
		body.collision.disabled = true
		$"Forest/Unstable Ground A5-1".queue_free()
		$"Forest/Unstable Ground A5-2".queue_free()
		$"Forest/Unstable Ground A5-3".queue_free()
		$"Forest/Unstable Ground A5-4".queue_free()
		$"Forest/Trap Area 8".queue_free()

func _on_Finish_body_entered(body):
	if body == player:
		get_tree().change_scene("res://scene/End.tscn")
