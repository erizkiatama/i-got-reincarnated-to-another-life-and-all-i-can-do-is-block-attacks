extends RigidBody2D

const DAMAGE = 20

func _on_Effect_body_entered(body):
	if "Monster" in body.name:
		queue_free()
	if body.name == "Player":
		body.hp -= DAMAGE
		if body.hp <= 0:
			body.hp = 0
		queue_free()
		body.is_attacked = true
		body.timer.start()
