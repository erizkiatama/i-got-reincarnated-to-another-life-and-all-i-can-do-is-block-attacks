extends Node2D

onready var player = get_node("Player")
onready var text = get_node("Forest/RichTextLabel")
onready var dialog_box = get_node("Forest/Polygon2D")
onready var bat = get_node("Forest/Bat Monster")
onready var first_dialog_finished = get_node("Forest/First Dialog Finished")
onready var second_dialog_finished = get_node("Forest/Second Dialog Finished")
onready var player_sprite = get_node("Forest/Polygon2D/PlayerSprite")
onready var goddess_sprite = get_node("Forest/Polygon2D/GoddessSprite")
onready var potion = get_node("Forest/Polygon2D/Potion")

var first_dialog = ["We are here!", "...\nIn the middle of forest?",
"Umm... It seems I got the coordinate wrong... Hehehe :3", "ARE YOU KIDDING ME??", 
"Just transport us to the right place now.", "Uhh yeah about that...",
"I can only use teleportation once in 3 hours.", "...", "What kind of Goddess that have COOLDOWN ON THEIR SKILLS?",
"How rude! I AM a Goddess!", "Well it seems you have to get out of this forest by yourself.",
"Wait a minute, you push this situation to me. Don't you have responsibility?",
"I cannot meddle directly to this world, I am a Goddess after all.", 
"Huft, useless one for sure.", "Relax! With your power this forest is a small feat.",
"What, useless defensive power from useless Goddess' blessing? Wow yeah that help.",
"You are a RUDE one.", "Your defensive power can withstand any type of attack.", 
"If monster attacks you when defending, the monster took damage instead.",
"Let's just try it right now. You see that Bat over there?", 
"Try go near it, when it attacks press SPACE to defend.",
"Press? What are you talking about?", "Just do it will you? Now go!", "Uhh that useless Goddess.",
"Press SPACE to defend huh? I'll try..."]
var first_turn = [1,0,1,0,0,1,1,0,0,1,1,0,1,0,1,0,1,1,1,1,1,0,1,0,0]

var second_dialog = ["Woah!!", "It seems you got the gist of it.", "Hmm, yeah I kind of get it somehow",
"But what happened if I'm not defending when the monster attacks?", 
"You will take damage, of course.", "You are pretty weak when not defending, so watch out for your health.",
"Okay, I will be careful.", "But if you are defending you never take damage... I guess? So relax!",
"Somehow you are not very convincing...", "Ah relax a bit will you?", "Oh yeah, I want to show you something",
"Could you go there to the ground that floating?", "Floating? What are you talking about?",
"Ah I call it floating ground but don't ask me why, ask the creator.", 
"Aren't you the creator? You said you ARE a Goddess.",
"Eh..? T-That's right, I am the creator. Ahahahahahah...", "You are weird.", "(That was close)",
"So I just have to go there right?", "Yes, of course.", "Okay then..."]
var second_turn = [0,1,0,0,1,1,0,1,0,1,1,1,0,1,0,1,0,1,0,1,0]

var third_dialog = ["...", "Amazing right? If you died I can ressurect you so no worries at all!",
"AT LEAST SAY SOMETHING WILL YOU?! I AM DEAD!!", "Well technically you WERE dead, because you are alive now!",
"THAT'S NOT THE POINT YOU USELESS GODDESS!!", "Jeez, relax a bit will you?", "Oh yeah by the way...",
"I only have the energy to ressurect you 4 times, more than that... Well, game over I guess?",
"Ah I ressurected you just now so only 3 times more.", "Convenient right?", "......",
"I KNEW IT, YOU ARE REALLY USELESS!!", 
"Why didn't you just tell me how it works? I don't have to sacrifice 1 lives for that!!",
"Well I thought you won't understand if I didn't do that...", "I give up...",
"Oh Relax! I will help you one more time!", "What are you up to this time..?",
"I will spread potion like this that will help you to get out from this forest.",
"Ahh finally you do something right and useful.", "~~~ TRALALA LILI LULU ~~~",  "Oh no...", "I accidentally mixed it up with spoilt ones",
"I knew it I say too much... Useless will always useless...", "How rude! At least I tried!",
"Well now it's up to you to get out from here.", "My blessing will always with you.",
"She can't do anything right, can she?", "Oh well, let's get out of this forest."]
var third_turn = [0,1,0,1,0,1,1,1,1,1,0,0,0,1,0,1,0,1,0,1,1,1,0,1,1,1,0,0]

var index = 0
var bat_is_dead = false
var player_is_dead = false
var is_first_dialog = true
var is_second_dialog = false
var is_third_dialog = false

func _ready():
	player_sprite.hide()
	text.set_bbcode(first_dialog[index])
	text.set_visible_characters(0)
	set_process_input(true)
	
func _process(delta):
	if !bat_is_dead:
		if bat.lives == 0:
			bat_is_dead = true
			first_dialog_finished.start()
	
	if player_is_dead:
		player_is_dead = false
		second_dialog_finished.start()

	
func _input(event):
	if (event is InputEventMouseButton && event.is_pressed()) || (event is InputEventKey && event.scancode == KEY_SPACE && event.is_pressed()):
		if text.get_visible_characters() > text.get_total_character_count():
			if index < first_dialog.size() - 1 && is_first_dialog:
				index += 1
				if first_turn[index] == 0:
					player_sprite.show()
					goddess_sprite.hide()
				else:
					player_sprite.hide()
					goddess_sprite.show()
				text.set_bbcode(first_dialog[index])
				text.set_visible_characters(0)
			elif index == first_dialog.size() - 1 && is_first_dialog:
				index = 0
				is_first_dialog = false
				dialog_box.hide()
				text.hide()
				player.show()
			elif index < second_dialog.size() - 1 && is_second_dialog:
				index += 1
				if second_turn[index] == 0:
					player_sprite.show()
					goddess_sprite.hide()
				else:
					player_sprite.hide()
					goddess_sprite.show()
				text.set_bbcode(second_dialog[index])
				text.set_visible_characters(0)
			elif index == second_dialog.size() - 1 && is_second_dialog:
				index = 0
				is_second_dialog = false
				dialog_box.hide()
				text.hide()
				player.show()
			elif index < third_dialog.size() - 1 && is_third_dialog:
				index += 1
				if third_turn[index] == 0:
					player_sprite.show()
					goddess_sprite.hide()
				else:
					player_sprite.hide()
					goddess_sprite.show()
				if "potion" in third_dialog[index]:
					potion.show()
				else:
					potion.hide()
				text.set_bbcode(third_dialog[index])
				text.set_visible_characters(0)
			elif index == third_dialog.size() - 1 && is_third_dialog:
				get_tree().change_scene("res://scene/World.tscn")
		else:
			text.set_visible_characters(text.get_total_character_count())

func _on_Timer_timeout():
	text.set_visible_characters(text.get_visible_characters() + 1)

func _on_First_Dialog_Finished_timeout():
	text.set_bbcode(second_dialog[index])
	text.set_visible_characters(0)
	player.hide()
	dialog_box.show()
	text.show()
	is_second_dialog = true

func _on_Trap_Area_body_entered(body):
	if body == player:
		$"Forest/Unstable Ground".queue_free()
		$"Forest/Unstable Ground 2".queue_free()
		$"Forest/Trap Area".queue_free()
		player_is_dead = true
		player.hp = 0
		player.after_death.start()

func _on_Second_Dialog_Finished_timeout():
	text.set_bbcode(third_dialog[index])
	text.set_visible_characters(0)
	player.hide()
	dialog_box.show()
	text.show()
	is_third_dialog = true
	