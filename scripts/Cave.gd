extends Node2D

onready var dialog_box = get_node("Dialog Box")
onready var text = get_node("Dialog Box/RichTextLabel")
onready var player_sprite = get_node("Dialog Box/Player Sprite")
onready var goddess_sprite = get_node("Dialog Box/Goddess Sprite")
var player

var is_finished = false

var dialog = ["This is...?", "I can't believe it...", "That's the legendary sword, Excalibur!",
"It said that it can kill a dragon in one slice.", "Woah, isn't this like.. SUPER COOL STUFF?!",
"I'll take this!", "With this I can get out of this forest more quickly!", 
"...", "Umm, yeah about that...?", "You did not forget that you can attack monster, right?",
"Eh?", "I said it in the beginning right?", 
"Because of my blessing, you can't attack any monsters with any kind of weapon.",
"Even if it is strongest weapon ever made.", "So... it means...",
"Yep", "This sword... is...", "useless...!", "Useless..!!", "USELESS!!!"
,"Yeah, that's right", "Ahhh to think that I've found some legendary weapon but I can't even use it",
"Let just take it, you can sell it later in the town.", "WHAT KIND OF PEOPLE SELL AN EXCALIBUR?!",
"But oh well, I take it just in case...", "Let's get out of here."]
var turn = [0,1,1,1,0,0,0,1,1,1,0,1,1,1,0,1,0,0,0,0,1,0,1,0,0,0]
var index = 0

func _input(event):
	if (event is InputEventMouseButton && event.is_pressed()) || (event is InputEventKey && event.scancode == KEY_SPACE && event.is_pressed()):
		if text.get_visible_characters() > text.get_total_character_count():
			if index < dialog.size() - 1:
				index += 1
				show_sprites(turn)
				text.set_bbcode(dialog[index])
				text.set_visible_characters(0)
			elif index == dialog.size() - 1:
				index = 0
				dialog_box.hide()
				text.hide()
				player.show()
				player.is_talking = false
				is_finished = true
		else:
			text.set_visible_characters(text.get_total_character_count())

func _on_Dialog_Interval_timeout():
	text.set_visible_characters(text.get_visible_characters() + 1)

func show_sprites(turn):
	if turn[index] == 0:
		player_sprite.show()
		goddess_sprite.hide()
	else:
		player_sprite.hide()
		goddess_sprite.show()

func _on_Treasure_body_entered(body):
	if body.name == "Player":
		player = body
		player.hide()
		player.is_talking = true
		dialog_box.show()
		text.show()
		text.set_bbcode(dialog[index])
		text.set_visible_characters(0)
		show_sprites(turn)
		$Treasure.queue_free()

func _on_Entrance_body_entered(body):
	if body.name == "Player" && is_finished:
		body.get_parent().get_node("Forest").show()
		body.global_position = $CaveDialogBoxSpawn.global_position
		queue_free()
