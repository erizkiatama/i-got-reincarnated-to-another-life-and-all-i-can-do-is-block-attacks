extends RichTextLabel

onready var player = get_parent().get_node("Player")
onready var goddess = get_parent().get_node("Goddess")

var dialog = ["This is it! I finally get out of this forest!",
"And it all because of my blessing!", "Uh whatever, at least I finally get out!",
"Hey, I can see the town! Finally, people! Not monsters!", 
"But it seems weird...", "Hmm? What are you talking abo-", "KYAAAAAAAAAA!!!!!", "Eh?",
"What is going on there?!", "Monsters? But how?!", "There's no time talking, I've got to help them!"
]

var turn = [0,1,0,0,1,0,2,0,0,1,0]

var index = 0

func _ready():
	set_bbcode(dialog[index])
	goddess.hide()
	set_visible_characters(0)
	set_process_input(true)
	
func _input(event):
	if (event is InputEventMouseButton && event.is_pressed()) || (event is InputEventKey && event.scancode == KEY_SPACE && event.is_pressed()):
		if get_visible_characters() > get_total_character_count():
			if index < dialog.size() - 1:
				index += 1
				if turn[index] == 0:
					player.show()
					goddess.hide()
				elif turn[index] == 1:
					player.hide()
					goddess.show()
				else:
					player.hide()
					goddess.hide()
				set_bbcode(dialog[index])
				set_visible_characters(0)
			elif index == dialog.size() - 1:
				get_parent().get_node("DialogBox").hide()
				get_parent().get_node("Text").hide()
				get_parent().get_node("Label").show()
				get_parent().get_node("The End").start()
				player.hide()
		else:
			set_visible_characters(get_total_character_count())

func _on_Timer_timeout():
	set_visible_characters(get_visible_characters() + 1)

func _on_The_End_timeout():
	get_tree().change_scene("res://scene/Menu.tscn")
