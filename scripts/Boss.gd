extends KinematicBody2D

const ACCELERATION = 70
const GRAVITY = 20
const ATTACK = 30
const ATTACK_DEFENDING = 1

var motion = Vector2.ZERO
onready var player = get_parent().get_parent().get_node("Player")
onready var sprite = get_node("AnimatedSprite")
onready var attack_animation = get_node("AttackAnimation")
onready var collision = get_node("CollisionShape2D")

var lives = 50
var is_entered = false
var is_attacking = false
var is_knocked = false
var is_dead = false

func _physics_process(delta):
	if !is_knocked && !is_dead:
		motion.y += GRAVITY
		if is_entered:
			$AnimatedSprite.play("move")
			if player.position.x < position.x:
				motion.x = -ACCELERATION
				$AnimatedSprite.flip_h = false
			elif player.position.x > position.x:
				motion.x = ACCELERATION
				$AnimatedSprite.flip_h = true
			else:
				$AnimatedSprite.play("idle")
				motion.x = 0
		elif is_attacking:
			$AnimatedSprite.play("attack")
		elif !is_attacking && !is_knocked:
			$AnimatedSprite.play("idle")
			motion.x = 0
	elif is_knocked && !is_dead:
		$AnimatedSprite.play("damaged")
		motion.x = 0
	elif is_knocked && is_dead:
		$AnimatedSprite.play("dead")
		motion = Vector2.ZERO
		$Attack/CollisionShape2D.disabled = true
		$Awareness/CollisionShape2D.disabled = true
		$CollisionShape2D.disabled = true
		
	motion = move_and_slide(motion)

func _on_Awareness_body_entered(body):
	if body.name == player.name && lives > 0:
		is_entered = true

func _on_Awareness_body_exited(body):
	if body.name == player.name && lives > 0:
		is_entered = false

func _on_Attack_body_entered(body):
	if body.name == player.name && lives > 0:
		is_attacking = true
		is_entered = false
		$AnimatedSprite.play("attack")
		if $AnimatedSprite.flip_h:
			motion.x = 150
		else:
			motion.x = -150
		if body.is_defending:
			dead(body.DEFEND)
		attack_animation.set_wait_time(0.1)
		attack_animation.start()

func _on_AttackAnimation_timeout():
	is_attacking = false
	is_entered = true
	is_knocked = false
	
func dead(damage):
	if $AnimatedSprite.flip_h:
		motion.x = -3000
	else:
		motion.x = 3000
	motion = move_and_slide(motion)
	
	is_attacking = false
	is_entered = false
	is_knocked = true
	lives -= damage
	if(lives <= 0):
		is_dead = true
	