extends RichTextLabel

onready var player = get_parent().get_node("Player")
onready var goddess = get_parent().get_node("Goddess")

var dialog = ["Where... is this...?",
"Welcome to afterlife, I am called Goddess Lilu.",
"Huh..? Why am I here...?",
"You are accidentally dead and I took your soul here.",
"I am about to reincarnate you to my world right now.",
"Are you serious? Is this some kind of joke right?", "I am... dead?",
"I am sorry, but this is the truth young man. Ah your name is Tama right?",
"Yes it is...", "Oh well I guess I have to accept it", "You said you were about to reincarnate me, right?",
"Yes! I will reincarnate you to my world, a fantasy-like world like you always wanted!",
"I don't expect it will become real but oh well...", 
"So... just to be clear though, you WILL give me at least a cheat ability right?",
"Well of course! I will give you my blessing!", "Cool! What kind of blessing is it? Oh man I cannot wait!",
"Let's just get into it, I will give you my blessing now", "~~~TRALALA LILI LULU~~~", "(Finally, cheat abilites!)",
"Huh?", "Eh?", "Uhhh...", "What's wrong? You said you are going to give me blessing, right? Is it done?",
"Ahh yeah about that... There are good news and bad news though...", "Eh? The good news is...?",
"Yeah, I successfully give you my blessing! You now can withstand any kind of attacks as long you are in defending state.",
"Cool!! And the bad news is?", "Uh yeah well...", 
"You cannot attack anything, with any kind of weapon or magic. Even it is a strongest type of weapon or magic.",
"Huh?", "What's more, you cannot escape from monster. You cannot jump through it, slide through it, or anything.",
"You can only defending, block the monster attacks.", "It is cool! Right? Hehehehe....",
"W-What....", "ISN'T THAT TOTALLY USELESS BLESSINGS?!", "Hey! At least I gave you something!", 
"NO YOU ARE NOT!!", "Now now let's not fret about past shall we? It is time to reincarnate you to the new world.",
"My cheat abilities turn out to be useless.....", "Oh come on, it is not that bad.", "Now I will reincarnate you to my world",
"~~~ TRALALA LILI LULU ~~~"]

var turn = [0,1,0,1,1,0,0,1,0,0,0,1,0,0,1,0,1,1,0,1,0,1,0,1,0,1,0,1,1,0,1,1,1,0,0,1,0,1,0,1,1,1]

var index = 0

func _ready():
	set_bbcode(dialog[index])
	goddess.hide()
	set_visible_characters(0)
	set_process_input(true)
	
func _input(event):
	if (event is InputEventMouseButton && event.is_pressed()) || (event is InputEventKey && event.scancode == KEY_SPACE && event.is_pressed()):
		if get_visible_characters() > get_total_character_count():
			if index < dialog.size() - 1:
				index += 1
				if turn[index] == 0:
					player.show()
					goddess.hide()
				else:
					player.hide()
					goddess.show()
				set_bbcode(dialog[index])
				set_visible_characters(0)
			elif index == dialog.size() - 1:
				get_tree().change_scene("res://scene/Tutorial.tscn")
		else:
			set_visible_characters(get_total_character_count())

func _on_Timer_timeout():
	set_visible_characters(get_visible_characters() + 1)
