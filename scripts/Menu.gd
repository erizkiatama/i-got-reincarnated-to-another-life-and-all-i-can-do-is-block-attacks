extends Node2D

onready var start_icon = get_node("Start Icon")
onready var quit_icon = get_node("Quit Icon")
	
func _physics_process(delta):
	if $MarginContainer/VBoxContainer/Start.is_hovered():
		start_icon.show()
		quit_icon.hide()
	elif $MarginContainer/VBoxContainer/Quit.is_hovered():
		start_icon.hide()
		quit_icon.show()
	else:
		start_icon.hide()
		quit_icon.hide()

func _on_Start_pressed():
	get_tree().change_scene("res://scene/Confirm.tscn")
	
func _on_Quit_pressed():
	get_tree().quit()
