extends KinematicBody2D

const UP = Vector2(0, -1)
const ACCELERATION = 50
const MAX_SPEED = 250
const JUMP_HEIGHT = -550
const GRAVITY = 20
const DEFEND = 1

onready var sprite = get_node("AnimatedSprite")
onready var timer = get_node("Timer")
onready var after_death = get_node("AfterDeath")
var motion = Vector2()

var lives = 3
var hp = 50
var is_defending = false
var is_attacked = false
var is_dead = false
var is_talking = false

func _physics_process(delta):
	motion.y += GRAVITY
	var friction = false
	if is_dead:
		$AnimatedSprite.play("damaged")
	else:
		if !is_talking:
			if Input.is_action_pressed("ui_right"):
				motion.x = min(motion.x + ACCELERATION, MAX_SPEED)
				$AnimatedSprite.flip_h = false
				$AnimatedSprite.play("run")
			elif Input.is_action_pressed("ui_left"):
				motion.x = max(motion.x - ACCELERATION, -MAX_SPEED)
				$AnimatedSprite.flip_h = true
				$AnimatedSprite.play("run")
			elif Input.is_action_pressed("ui_select"):
				$AnimatedSprite.play("defend")
				friction = true
				is_defending = true
			else:
				if is_attacked:
					$AnimatedSprite.play("damaged")
				else:
					$AnimatedSprite.play("idle")
				friction = true
				is_defending = false
		else:
			motion.x = 0
		
		if is_on_floor():
			if Input.is_action_just_pressed("ui_up"):
				motion.y = JUMP_HEIGHT
			if friction == true:
				motion.x = lerp(motion.x, 0, 0.2)
		else:
			if motion.y < 0:
				$AnimatedSprite.play("jump")
			else:
				$AnimatedSprite.play("fall")
			if friction == true:
				motion.x = lerp(motion.x, 0, 0.05)
		
		if self.global_position.y > 650 && !is_dead:
			lives-=1
			hp = 0
			is_dead = true
			after_death.start()
	
	motion = move_and_slide(motion, UP)

func _on_Attacker_body_entered(body):
	if "Monster" in body.name && !is_defending && body.lives > 0:
		is_attacked = true
		hp -= body.ATTACK
		if hp <= 0:
			hp = 0
			is_dead = true
			lives -= 1
			after_death.start()
			return
		if "Boss" in body.name:
			if body.sprite.flip_h:
				motion.x = 800
			else:
				motion.x = -800
		else:
			if body.sprite.flip_h:
				motion.x = -800
			else:
				motion.x = 800
		timer.start()
		
	elif "Monster" in body.name && is_defending && not "Boss" in body.name:
		is_attacked = false
	elif "Boss" in body.name && is_defending:
		is_attacked = true
		hp -= body.ATTACK_DEFENDING
		if hp <= 0:
			hp = 0
			is_dead = true
			lives -= 1
			after_death.start()
			return
		timer.start()

func _on_Timer_timeout():
	is_attacked = false

func _on_AfterDeath_timeout():
	if lives > 0:
		self.global_position = get_parent().get_node("Forest/PlayerSpawnPoint").global_position
		is_dead = false
		is_attacked = false
		hp = 50
	else:
		get_tree().change_scene("res://scene/Menu.tscn")